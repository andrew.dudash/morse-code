int led_pin = LED_BUILTIN;

void setup() {
  pinMode(led_pin, OUTPUT);
}

int DOT_DELAY = 10;
int DASH_DELAY = DOT_DELAY * 3;
int WAIT_DELAY = DOT_DELAY;

void dot() {
  digitalWrite(led_pin, HIGH);
  delay(DOT_DELAY);;
  digitalWrite(led_pin, LOW);
  delay(WAIT_DELAY);
}

void dash() {
  digitalWrite(led_pin, HIGH);
  delay(DASH_DELAY);
  digitalWrite(led_pin, LOW);
  delay(WAIT_DELAY);
}

char * morse_code[] = {
  "A.-",
  "B-...",
  "C-.-.",
  "D-..",
  "E.",
  "F..-.",
  "G--.",
  "H....",
  "I..",
  "J.---",
  "K-.-",
  "L.-..",
  "M--",
  "N-.",
  "O---",
  "P.--.",
  "Q--.-",
  "R.-.",
  "S...",
  "T-",
  "U..-",
  "V...-",
  "W.--",
  "X-..-",
  "Y-.--",
  "Z--..",
  " "
};

void emit_array(char * buffer) {
  size_t index = 0;
  while (buffer[index]) {
    if (buffer[index] == '.') {
      dot();
    } else if (buffer[index] == '-') {
      dash();
    }
    index++;
  }
}

void emit_letter(char letter) {
  for (size_t index = 0; index < sizeof(morse_code); index++) {
    if (morse_code[index][0] == letter) {
      emit_array(morse_code[index] + 1);
      return;
    }
  }
}

void emit_text(const char * buffer) {
  size_t index = 0;
  while (buffer[index]) {
    emit_letter(buffer[index++]);
    delay(WAIT_DELAY * 2);
  }
}

void loop() {
  delay(WAIT_DELAY * 10);
  emit_text("BEEP BOOP");
}